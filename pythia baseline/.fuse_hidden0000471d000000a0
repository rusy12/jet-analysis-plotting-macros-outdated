Double_t PythiaFitFunc(Double_t* x_val, Double_t* par)
{
    // Fit function for d2N/(2pi*pT dpT dy)
    // taken from here: http://sampa.if.usp.br/~suaide/blog/files/papers/PLB6372006.pdf
    Double_t pT, y,y1,y2, B1, T1, n1, m1, mu1,B2, T2, n2, m2, mu2;
    B1    = par[0];
    T1    = par[1];
    n1    = par[2];
    m1   = par[3];
	 mu1  = par[4];
	 B2    = par[5];
    T2    = par[6];
    n2    = par[7];
    m2   = par[8];
	 mu2   = par[9];

	 pT=x_val[0];
	 
    Double_t mT1 = TMath::Sqrt((pT-mu1)*(pT-mu1)+m1*m1);
	 Double_t mT2 = TMath::Sqrt((pT-mu2)*(pT-mu2)+m2*m2);
	 
	y1 =B1/TMath::Power(1.0+(mT1-m1)/(n1*T1),n1);
	y2=B2/TMath::Power(1.0+(mT2-m2)/(n2*T2),n2);
	
	
	
	if(pT<15)
		y=y1;
	else if(pT<25)
	{
		Double_t c1=(pT-15.0)/10.0;
		y=((1-c1)*y1+c1*y2);
	}
	 else y=y2;

	 return y;
}

void plot_my_plots(TString suffix="_effionly")
{
	
	gStyle->SetOptStat(0);
   gStyle->SetPalette(1);
   gStyle->SetOptDate(0);
   gStyle->SetPadLeftMargin(0.12);
   gStyle->SetPadRightMargin(0.09);
   gStyle->SetPadTopMargin(0.1);
   gStyle->SetPadBottomMargin(0.15);
   gStyle->SetTitleSize(0.055,"Y");
   gStyle->SetTitleOffset(0.95,"Y");
   gStyle->SetTitleSize(0.06,"X");
   gStyle->SetTitleOffset(0.95,"X");
   gStyle->SetLabelSize(0.03,"X");
   gStyle->SetLabelSize(0.03,"Y");
  
  
   //canvas size
   Int_t can_x=1200; //1600
   Int_t can_y=680; //900
    
  Float_t spectraXmin=0;
  Float_t spectraXmax=50;
  Float_t spectraYmin=1E-12;
  Float_t spectraYmax=1E0;
  
	TString name=Form("histos_pythiajet_R0.3%s.root",suffix.Data());
	TFile *infile = new TFile(name.Data(),"OPEN");
	TH1D* hptl0=(TH1D*) infile->Get("hpT_pTl0");
	TH1D* hptl5=(TH1D*) infile->Get("hpT_pTl5");
	TH1D* hptl5_dete=(TH1D*) infile->Get("hpT_pTl5_dete");
	
	TH1I* hevents=(TH1I*) infile->Get("hevts");
	Int_t nevents=hevents->GetEntries();
	
	name="histos_pythiajet_R0.3_effi_pTsmear.root";
	TFile *infile2 = new TFile(name.Data(),"OPEN");
	TH1D* hptl5_dete2=(TH1D*) infile2->Get("hpT_pTl5_dete");

	
	hptl0->Scale(1./nevents);
	hptl5->Scale(1./nevents);
	hptl5_dete->Scale(1./nevents);
	hptl5_dete2->Scale(1./nevents);

	hptl0->Rebin(4);
	hptl5->Rebin(4);
	hptl5_dete->Rebin(4);
	hptl5_dete2->Rebin(4);
	
	hptl0->SetLineColor(kBlack);
	hptl5->SetLineColor(kRed);
	hptl5_dete->SetLineColor(kBlue);
	hptl5_dete2->SetLineColor(kGreen+1);
	
	hptl0->SetLineWidth(2);
	hptl5->SetLineWidth(2);
	hptl5_dete->SetLineWidth(2);
	hptl5_dete2->SetLineWidth(2);

	
	TH1 *frame = new TH1I("frame", "", 1000, -100, +100);

   TCanvas *spectra = new TCanvas("cspectra","cspectra",10,10,can_x,can_y);
   cspectra->cd();
   cspectra->SetGrid();
   cspectra->SetLogy();
   frame->GetXaxis()->SetTitle("p_{T} (GeV/c)");
   frame->GetYaxis()->SetTitle("1/N_{events} d^{2}N/dp_{T}d#eta 1/2#pi (1/GeV)");
   frame->GetXaxis()->SetRangeUser(spectraXmin, spectraXmax);
   frame->GetYaxis()->SetRangeUser(spectraYmin, spectraYmax);
   //frame->GetYaxis()->SetRangeUser(1E-3, 1E5);
   TString str="PYTHIA inclusive jet spectrum";
	//TString str="PYTHIA jet spectrum - highest p_{T} jets only";

   frame->SetTitle(str);
   frame->DrawCopy("");
   hptl0->Draw("same");
   hptl5->Draw("same");
   hptl5_dete->Draw("same");
   hptl5_dete2->Draw("same");

  TLegend *legspectra = new TLegend(0.6467, 0.60, 0.89, 0.90);
  legspectra->SetTextSize(0.03);
  legspectra->SetFillStyle(0);
  legspectra->SetBorderSize(0);
  legspectra->AddEntry(hptl0,"p_{T}^{lead}>0GeV","l");
  legspectra->AddEntry(hptl5,"p_{T}^{lead}>5GeV","l");
  legspectra->AddEntry(hptl5_dete,"p_{T}^{lead}>5GeV, trk. eff. applied","l");
  legspectra->AddEntry(hptl5_dete2,"p_{T}^{lead}>5GeV, trk. eff. and p_{T} smearing","l");
  legspectra->DrawClone("same");
}

void compare_with_matt(Float_t R=0.3, Float_t pTlead=5, TString suffix="")
{
	gStyle->SetOptStat(0);
   gStyle->SetPalette(1);
   gStyle->SetOptDate(0);
   gStyle->SetPadLeftMargin(0.12);
   gStyle->SetPadRightMargin(0.09);
   gStyle->SetPadTopMargin(0.1);
   gStyle->SetPadBottomMargin(0.15);
   gStyle->SetTitleSize(0.055,"Y");
   gStyle->SetTitleOffset(0.95,"Y");
   gStyle->SetTitleSize(0.06,"X");
   gStyle->SetTitleOffset(0.95,"X");
   gStyle->SetLabelSize(0.03,"X");
   gStyle->SetLabelSize(0.03,"Y");
  
  
   //canvas size
   Int_t can_x=1200; //1600
   Int_t can_y=680; //900
    
  Float_t spectraXmin=0;
  Float_t spectraXmax=50;
  Float_t spectraYmin=1E-13;
  Float_t spectraYmax=1E-1;
  
  //my PYTHIA
	TString name=Form("histos_pythiajet_R%.1lf%s.root",R,suffix.Data());
	TFile *infile = new TFile(name.Data(),"OPEN");
	TH1D* hptl=(TH1D*) infile->Get(Form("hpT_pTl%.0lf",pTlead));
	hptl->SetLineColor(kRed);
	hptl->SetMarkerStyle(29);
   hptl->SetLineWidth(2);
   hptl->SetMarkerColor(kRed);
	hptl->SetMarkerSize(0.9);
	
	TH1I* hevents=(TH1I*) infile->Get("hevts");
	Int_t nevents=hevents->GetEntries();
	
	Float_t ppXsection=42;
	Float_t jetscale=ppXsection/(nevents);
   //hptl->Rebin(4);
	hptl->Scale(jetscale);
	

  TString str = "PythiaHistosChargedJets2.root";
  TFile *f = new TFile(str.Data(), "OPEN");

//MATT PYTHIA
  str=Form("h_PythiaJetPt0p%.0lfTotpTLeading%.0lf",R*10,pTlead);
  TH1D* hPythia=(TH1D*) f->Get(str);
  hPythia->SetLineColor(kBlack);
  hPythia->SetMarkerStyle(29);
  hPythia->SetLineWidth(2);
  hPythia->SetMarkerColor(kBlack);
  hPythia->SetMarkerSize(1.0);
  
  TH1D* hdiv=(TH1D*) hptl->Clone("hdiv");
  hdiv->Reset("MICE");
  
  
  
  for(int pT=1;pT<50;pT++)
  {
	  
	  int bin1=hPythia->GetXaxis()->FindBin(pT);
	  int bin2=hptl->GetXaxis()->FindBin(pT);
	  if(hPythia->GetBinContent(bin1)==0)continue;
	  float div=hptl->GetBinContent(bin2)/hPythia->GetBinContent(bin1);
	  cout<<"pT :"<<pT<<" jan/matt: "<<div<<endl;
  }
  
  
  
  TH1 *frame = new TH1I("frame", "", 1000, -100, +100);

  TCanvas *spectra = new TCanvas("cspectra","cspectra",10,10,can_x,can_y);
  cspectra->cd();
  cspectra->SetGrid();
  cspectra->SetLogy();
  frame->GetXaxis()->SetTitle("p_{T} (GeV/c)");
  frame->GetYaxis()->SetTitle("d#sigma/dp_{T} (mb/GeV)");
  frame->GetXaxis()->SetRangeUser(spectraXmin, spectraXmax);
  frame->GetYaxis()->SetRangeUser(spectraYmin, spectraYmax);
  //frame->GetYaxis()->SetRangeUser(1E-3, 1E5);
  str=Form("PYTHIA charged jets w/ p_{T}^{lead}>%.0lfGeV/c, R=%.1lf",pTlead,R);
  frame->SetTitle(str);
  frame->DrawCopy("");
  hPythia->DrawCopy("E same");
  hptl->Draw("E same");
  
    TLegend *legspectra = new TLegend(0.6467, 0.60, 0.89, 0.90);
  legspectra->SetTextSize(0.03);
  legspectra->SetFillStyle(0);
  legspectra->SetBorderSize(0);
  legspectra->AddEntry(hPythia,"Matt","lp");
  legspectra->AddEntry(hptl,"Jan","lp");
  legspectra->DrawClone("same");
  
    TString str=Form("obr/PYTHIA_Jan_vs_Matt_R%.1lf_pTlead%0.lf.gif",R,pTlead);
  cspectra->SaveAs(str.Data());
  /*
  TCanvas *spectra2 = new TCanvas("cspectra2","cspectra2",10,10,can_x,can_y);
  cspectra2->cd();
  cspectra->SetGrid();
  hdiv->Draw();
  */
  


  
}

void compare_with_star(Float_t R=0.6, Float_t pTlead=0, TString suffix="_full",TString ext="gif")
{
		gStyle->SetOptStat(0);
   gStyle->SetPalette(1);
   gStyle->SetOptDate(0);
   gStyle->SetPadLeftMargin(0.12);
   gStyle->SetPadRightMargin(0.09);
   gStyle->SetPadTopMargin(0.1);
   gStyle->SetPadBottomMargin(0.15);
   gStyle->SetTitleSize(0.055,"Y");
   gStyle->SetTitleOffset(0.95,"Y");
   gStyle->SetTitleSize(0.06,"X");
   gStyle->SetTitleOffset(0.95,"X");
   gStyle->SetLabelSize(0.03,"X");
   gStyle->SetLabelSize(0.03,"Y");
  
  
   //canvas size
   Int_t can_x=1200; //1600
   Int_t can_y=680; //900
    
  Float_t spectraXmin=0;
  Float_t spectraXmax=50;
  Float_t spectraYmin=1E-10;
  Float_t spectraYmax=1E0;
  
  TString outdir="../obr/";
  
   TF1* fjet1 = new TF1("fjet1",PythiaFitFunc,0.0,50.0,10);
	fjet1->SetParameter(0,1); // B, changes the amplitude, 0.1
	fjet1->SetParameter(1,0.99); // T, changes slope, 0.4
	fjet1->SetParameter(2,18.1); // n, changes how fast spectrum drops, 5.8
	fjet1->SetParameter(3,0.000001); // m0, changes the width, 0.0001		
	fjet1->SetParameter(4,-4.7); // mu, changes the x-axis shift
	fjet1->SetParameter(5,1); // B, changes the amplitude, 0.1
	fjet1->SetParameter(6,0.99); // T, changes slope, 0.4
	fjet1->SetParameter(7,18.1); // n, changes how fast spectrum drops, 5.8
	fjet1->SetParameter(8,0.000001); // m0, changes the width, 0.0001		
	fjet1->SetParameter(9,-4.7); // mu, changes the x-axis shift
	fjet1->SetRange(0,50);
	fjet1->SetLineWidth(2);
	fjet1->SetLineStyle(1);
	fjet1->SetLineColor(kMagenta);
	fjet1->SetNpx(1000);
  
  //my PYTHIA
	TString name=Form("histos_pythiajet_R%.1lf%s.root",R,suffix.Data());
	TFile *infile = new TFile(name.Data(),"OPEN");
	TH1D* hptl=(TH1D*) infile->Get(Form("hpT_pTl%.0lf",pTlead));
	hptl->SetLineColor(kRed);
	hptl->SetMarkerStyle(22);
   hptl->SetLineWidth(2);
   hptl->SetMarkerColor(kRed);
	hptl->SetMarkerSize(1.0);
	
	TH1I* hevents=(TH1I*) infile->Get("hevts");
	Int_t nevents=hevents->GetEntries();
	
	Float_t ppXsection=42;
	Float_t jetscale=ppXsection/(nevents);
  	hptl->Scale(jetscale);
	hptl->Rebin(8);
	hptl->Scale(1./8);
	hptl->Fit("fjet1","R");
	
	TH1 *frame = new TH1I("frame", "", 1000, -100, +100);
 
	TCanvas *spectra = new TCanvas("cspectra","cspectra",10,10,can_x,can_y);
   cspectra->cd();
   cspectra->SetGrid();
	
   Double_t eps=0.02;
   TPad* p1 = new TPad("p1","p1",0,0.35-eps,1,1,0); p1->Draw();
   p1->SetBottomMargin(eps);
	p1->SetGrid();
	p1->SetLogy();
	
	TPad* p2 = new TPad("p2","p2",0,0,1,0.35*(1.-eps),0); p2->Draw(); 
	p2->SetTopMargin(0);
	p2->SetBottomMargin(0.25);
	p2->SetGrid();
	p2->SetFillColor(0);
   p2->SetFillStyle(0);
	
   p1->cd();
	
  //cspectra->SetLogy();
  //frame->GetXaxis()->SetTitle("p_{T} (GeV/c)");
  //frame->GetYaxis()->SetTitle("d#sigma/dp_{T} (#mub/GeV)");
  frame->GetXaxis()->SetRangeUser(spectraXmin, spectraXmax);
  frame->GetYaxis()->SetRangeUser(spectraYmin, spectraYmax);
  //frame->GetYaxis()->SetRangeUser(1E-3, 1E5);
  TString str="";//Form("PYTHIA vs STAR, full jets, R=%.1lf",R);
  //frame->SetTitle(str);
  frame->DrawCopy("");
fjet1->Draw("same");

  //STAR 2009 pp data
  const Int_t nbins=10;
  double avalx[nbins]={10.7032,12.6309,14.9538,17.532,20.5296,24.2735,28.6826,33.7055,39.6978,46.6486};
  double avaly[nbins]={1.81048,0.518655,0.180917,0.0560875,0.0170113,0.00477476,0.00123286,0.000272281, 5.28999e-05,7.37398e-06};

  double aexl[nbins];
  double aexh[nbins];
  double aeyl_stat[nbins]={0.0754247,0.0141737,0.00341262,0.00121029,0.000238343,5.79578e-05,1.5516e-05,3.97909e-06,1.17925e-06,3.11505e-07};
  double aeyh_stat[nbins]={0.0754247,0.0141737,0.00341262,0.00121029,0.000238343,5.79578e-05,1.5516e-05,3.97909e-06,1.17925e-06,3.11505e-07};
  double aeyl_sys[nbins]={0.0876499,0.0402103,0.0175924,0.00620502,0.00214948,0.000574471,0.000188659,4.60589e-05,1.02032e-05,1.57978e-06};
  double aeyh_sys[nbins]={0.0876499,0.0402103,0.0175924,0.00620502,0.00214948,0.000574471,0.000188659,4.60589e-05,1.02032e-05,1.57978e-06};
  
  for(int bin=0; bin<nbins;bin++)
	  {

		  aexl[bin]=0.2;
		  aexh[bin]=0.2;
		  avaly[bin]=avaly[bin]/(1000*2*TMath::Pi());
		  aeyl_stat[bin]=aeyl_stat[bin]/(1000*2*TMath::Pi());
		  aeyh_stat[bin]=aeyh_stat[bin]/(1000*2*TMath::Pi());
		  aeyl_sys[bin]=aeyl_sys[bin]/(1000*2*TMath::Pi());
		  aeyh_sys[bin]=aeyh_sys[bin]/(1000*2*TMath::Pi());
	  }
  
  //STAR 2009 pp vs PYTHIA fit
  double avaly_vs_fit[nbins];
  double aeyl_stat_vs_fit[nbins];
  double aeyh_stat_vs_fit[nbins];
  double aeyl_sys_vs_fit[nbins];
  double aeyh_sys_vs_fit[nbins];
  for(int bn=0;bn<nbins;bn++)
  {
	  avaly_vs_fit[bn]=avaly[bn]/fjet1->Eval(avalx[bn]);
	  aeyl_stat_vs_fit[bn]=aeyl_stat[bn]/fjet1->Eval(avalx[bn]);
	  aeyh_stat_vs_fit[bn]=aeyh_stat[bn]/fjet1->Eval(avalx[bn]);
	  aeyl_sys_vs_fit[bn]=aeyl_sys[bn]/fjet1->Eval(avalx[bn]);
	  aeyh_sys_vs_fit[bn]=aeyh_sys[bn]/fjet1->Eval(avalx[bn]);
  }
  
  //STAR 2006 pp data
   const Int_t nbins2=9;
  double bvalx[nbins2]={8.3,10.3,12.6,15.5,19.0,23.4,28.7,35.3,43.3};
  double bvaly[nbins2]={6.4e05,2.4e05,5.9e04,1.19e04,3.5e03,7.8e02,1.41e02,2.2e01,2.6e00};
  double bexl[nbins2]={8.3-7.6,10.3-9.3,12.6-11.4,15.5-14.1,19.0-17.3,23.4-21.3,28.7-26.2,35.3-32.2,43.3-39.6};
  double bexh[nbins2]={9.3-8.3,11.4-10.3,14.1-12.6,17.3-15.5,21.3-19.0,26.2-23.4,32.2-28.7,39.6-35.3,48.7-43.3};
  double beyl_stat[nbins2]={0.9e05,0.3e05,0.6e04,0.1e04,0.2e03,0.4e02,0.09e02,0.3e01,0.6e00};
  double beyh_stat[nbins2]={0.9e05,0.3e05,0.6e04,0.1e04,0.2e03,0.4e02,0.09e02,0.3e01,0.6e00};
  double beyl_sys[nbins2]={3.1e05,1.1e05,2.8e04,0.57e04,1.7e03,3.7e02,0.68e02,1.1e01,1.3e00};
  double beyh_sys[nbins2]={3.1e05,1.1e05,2.8e04,0.57e04,1.7e03,3.7e02,0.68e02,1.1e01,1.3e00};
	
  for(int bin=0; bin<nbins2;bin++)
	  {

		  bvaly[bin]=bvaly[bin]/1E9;
		  beyl_stat[bin]=beyl_stat[bin]/1E9;
		  beyh_stat[bin]=beyh_stat[bin]/1E9;
		  beyl_sys[bin]=beyl_sys[bin]/1E9;
		  beyh_sys[bin]=beyh_sys[bin]/1E9;
	  }
	  
  

	
	TGraphAsymmErrors* gppjets06 = new TGraphAsymmErrors(nbins2, bvalx, bvaly, bexl, bexh, beyl_stat, beyh_stat);
   gppjets06->SetLineColor(kBlack);
   gppjets06->SetLineWidth(2);
	gppjets06->SetMarkerColor(kBlack);
	gppjets06->SetMarkerStyle(33);
	gppjets06->SetMarkerSize(2.0);
	gppjets06->GetXaxis()->SetLimits(spectraXmin,spectraXmax);
	gppjets06->GetHistogram()->SetMaximum(spectraYmax);   // along          
   gppjets06->GetHistogram()->SetMinimum(spectraYmin);
   gppjets06->Draw("ap");
	//gppjets06->SetTitle(Form("R_{AA}, R=%.1lf, p_{T}^{leading}>%.1lf GeV/c",R,pTthresh));
	gppjets06->SetTitle(str);
	gppjets06->GetXaxis()->SetTitle("p_{T,jet}^{full} (GeV/c)");
   gppjets06->GetYaxis()->SetTitle("d^{2}#sigma/(dp_{T}d#eta2#pi) (mb/GeV) ");
	//gppjets06->GetXaxis()->CenterTitle()
	gppjets06->GetYaxis()->CenterTitle();
	
	TGraphAsymmErrors* gppjet06_sys = new TGraphAsymmErrors(nbins2, bvalx, bvaly, bexl, bexh, beyl_sys, beyh_sys);
	gppjet06_sys->SetFillColor(kBlack);
   gppjet06_sys->SetLineColor(kBlack);
	gppjet06_sys->SetLineWidth(2);
   gppjet06_sys->SetFillStyle(0);
	gppjet06_sys->Draw("2");
		 
	hptl->Draw("E same");

	  
  TGraphAsymmErrors* gppjets09 = new TGraphAsymmErrors(nbins, avalx, avaly, aexl, aexh, aeyl_stat, aeyh_stat);
   gppjets09->SetLineColor(kBlue);
   gppjets09->SetLineWidth(2);
	gppjets09->SetMarkerColor(kBlue);
	gppjets09->SetMarkerStyle(29);
	gppjets09->SetMarkerSize(2.0);
	gppjets09->GetXaxis()->SetLimits(spectraXmin,spectraXmax);
	gppjets09->GetHistogram()->SetMaximum(spectraYmax);   // along          
   gppjets09->GetHistogram()->SetMinimum(spectraYmin);
   gppjets09->Draw("p");
	//gppjets09->SetTitle(Form("R_{AA}, R=%.1lf, p_{T}^{leading}>%.1lf GeV/c",R,pTthresh));
	gppjets09->SetTitle(str);
	gppjets09->GetXaxis()->SetTitle("p_{T,jet}^{full} (GeV/c)");
   gppjets09->GetYaxis()->SetTitle("d^{2}#sigma/(dp_{T}d#eta2#pi) (mb/GeV) ");
	//gppjets09->GetXaxis()->CenterTitle()
	gppjets09->GetYaxis()->CenterTitle();
	
	TGraphAsymmErrors* gppjet09_sys = new TGraphAsymmErrors(nbins, avalx, avaly, aexl, aexh, aeyl_sys, aeyh_sys);
	gppjet09_sys->SetFillColor(kBlue);
   gppjet09_sys->SetLineColor(kBlue);
	gppjet09_sys->SetLineWidth(2);
   gppjet09_sys->SetFillStyle(0);
	gppjet09_sys->Draw("2");
	  
	 double posx1=0.50;
    double posx2=0.90;
    double posy1=0.55;
    double posy2=0.85;
  
  TLegend *legraa = new TLegend(posx1,posy1,posx2,posy2);
  legraa->SetTextSize(0.035);
  legraa->SetFillStyle(0);
  legraa->SetBorderSize(0);
  legraa->AddEntry(gppjets09, "STAR pp #sqrt{s_{NN}}=200 GeV, 2009, anti-kT, R=0.6","lp");
  legraa->AddEntry(gppjets06, "STAR pp #sqrt{s_{NN}}=200 GeV, 2006, midcone, R=0.6", "lp");
  legraa->AddEntry(hptl, Form("PYTHIA8, R=%.1lf",R), "lp");
   // legraa->AddEntry(gtaa, "T_{AA} uncertainty", "f");
	legraa->DrawClone("same");
	
	
	//RATIO plot
	//*****************************
	  p2->cd();

   TGraphAsymmErrors* gpp09_vs_fit = new TGraphAsymmErrors(nbins, avalx, avaly_vs_fit, aexl, aexh, aeyl_stat_vs_fit, aeyh_stat_vs_fit);
   gpp09_vs_fit->SetLineColor(kBlue);
   gpp09_vs_fit->SetLineWidth(2);
	gpp09_vs_fit->SetMarkerColor(kBlue);
	gpp09_vs_fit->SetMarkerStyle(29);
	gpp09_vs_fit->SetMarkerSize(2.0);
	gpp09_vs_fit->GetXaxis()->SetLimits(spectraXmin,spectraXmax);
	gpp09_vs_fit->GetHistogram()->SetMaximum(1.5);   // along          
   gpp09_vs_fit->GetHistogram()->SetMinimum(0.5);
   gpp09_vs_fit->Draw("ap");
	//gpp09_vs_fit->SetTitle(Form("R_{AA}, R=%.1lf, p_{T}^{leading}>%.1lf GeV/c",R,pTthresh));
	gpp09_vs_fit->SetTitle("");
	gpp09_vs_fit->GetXaxis()->SetTitle("p_{T,jet}^{full} (GeV/c)");
   gpp09_vs_fit->GetYaxis()->SetTitle("STAR pp 2009 / PYTHIA fit");
	//gpp09_vs_fit->GetXaxis()->CenterTitle()
	gpp09_vs_fit->GetYaxis()->CenterTitle();
  gpp09_vs_fit->GetYaxis()->SetLabelSize(0.06);
  gpp09_vs_fit->GetXaxis()->SetLabelSize(0.06);
  gpp09_vs_fit->GetXaxis()->SetTitleSize(0.09);
  gpp09_vs_fit->GetYaxis()->SetTitleSize(0.06);
	
		TGraphAsymmErrors* gpp09_vs_fit_sys = new TGraphAsymmErrors(nbins, avalx, avaly_vs_fit, aexl, aexh, aeyl_sys_vs_fit, aeyh_sys_vs_fit);
	gpp09_vs_fit_sys->SetFillColor(kBlue);
   gpp09_vs_fit_sys->SetLineColor(kBlue);
	gpp09_vs_fit_sys->SetLineWidth(2);
   gpp09_vs_fit_sys->SetFillStyle(0);
	gpp09_vs_fit_sys->Draw("2");
	
		TLine *one = new TLine(spectraXmin, 1, spectraXmax, 1);
  one->SetLineWidth(3);
  one->SetLineStyle(2);
  one->SetLineColor(kBlack);
  one->DrawClone("same");
	
  str = Form("%s/all_R_toyspec_pTlead%.0lf.%s", outdir.Data(),pTlead,ext.Data());
  spectra->SaveAs(str.Data());
}