plot_measured()
{ 
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  gStyle->SetOptDate(0);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.09);
  gStyle->SetPadTopMargin(0.1);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetTitleSize(0.055,"Y");
  gStyle->SetTitleOffset(0.95,"Y");
  gStyle->SetTitleSize(0.06,"X");
  gStyle->SetTitleOffset(0.95,"X");
  gStyle->SetLabelSize(0.03,"X");
  gStyle->SetLabelSize(0.03,"Y");
  
  
  //canvas size
  Int_t can_x=1200; //1600
  Int_t can_y=680; //900
  Double_t pTcut=0;
  //Double_t R=0.3;
  TString trigger="MB";
  
    Float_t spectraXmin=0;
  Float_t spectraXmax=50;
  Float_t spectraYmin=1E-5;
  Float_t spectraYmax=1E5;
  

  TString str = "histos_pythiajet_R0.3.root";
  TFile *f = new TFile(str.Data(), "OPEN");


  TH2D* histo2d=(TH2D*) f->Get("hpT_pTlead");
  TH1D* hmeasured=histo2d->ProjectionX("histoold1",histo2d->GetYaxis()->FindBin(pTcut),histo2d->GetYaxis()->GetNbins());
  hmeasured->SetLineColor(kBlack);
   hmeasured->SetMarkerStyle(29);
  hmeasured->SetLineWidth(2);
  hmeasured->SetMarkerColor(kBlack);
  hmeasured->SetMarkerSize(1.0);
//hmeasured->Scale(10);

  
  TF1* fjet=new TF1("fjet","[0]*x/(TMath::Sqrt(x*x+[1]))*TMath::Power((1+x/[2]),[3])*(1/(1+TMath::Exp((x-15)/0.3)))+[4]*TMath::Exp(-[5]*x)*TMath::Power(x,-[6])*(1/(1+TMath::Exp((-x-15)/0.3)))",1,100);

  
  double p0 = 1.14094e+07;
  double p1 = 1.09762e+04;
  double p2=  5.58729e+04;
  double p3= -6.47004e+04;
  double p4=  2.92938e+04;
  double p5=  2.75701e-01;
  double p6=  1.95040e+00;

  fjet->SetParameters(p0,p1,p2,p3,p4,p5,p6);
  fjet->SetLineColor(kBlue);
  
  TF1* fjetold=new TF1("fjetold","[0]*TMath::Exp(-[1]*x)*TMath::Power(x,-[2])",1,100);
  
  double pa= 4.5e+03;
  double pb=0.29;
  double pc=1.25981;
  
  fjetold->SetParameters(pa,pb,pc);
  fjetold->SetLineColor(kRed);
  
    TH1 *frame = new TH1I("frame", "", 1000, -100, +100);

    TCanvas *spectra = new TCanvas("cspectra","cspectra",10,10,can_x,can_y);
  cspectra->cd();
  cspectra->SetGrid();
  cspectra->SetLogy();
  frame->GetXaxis()->SetTitle("p_{T} (GeV/c)");
  frame->GetYaxis()->SetTitle("counts");
  frame->GetXaxis()->SetRangeUser(spectraXmin, spectraXmax);
  frame->GetYaxis()->SetRangeUser(spectraYmin, spectraYmax);
  //frame->GetYaxis()->SetRangeUser(1E-3, 1E5);
  str="Toymodel hard jet spectrum";
  frame->SetTitle(str);
  frame->DrawCopy("");
  hmeasured->DrawCopy("E same");
  fjet->Draw("same");
  fjetold->Draw("same");
  
  TLegend *legspectra = new TLegend(0.6467, 0.60, 0.89, 0.90);
  legspectra->SetTextSize(0.03);
  legspectra->SetFillStyle(0);
  legspectra->SetBorderSize(0);
  legspectra->AddEntry(hmeasured,"Pyt. ch. jets w/ p_{T}^{leading}>7.0GeV/c","lp");
  //legspectra->AddEntry(fjet,"new fit","l");
  //legspectra->AddEntry(fjetold,"old fit","l");
  legspectra->DrawClone("same");
  
}
