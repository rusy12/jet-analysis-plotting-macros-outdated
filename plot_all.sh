#!/bin/bash
EVO="omicron"
LABEL="THIS THESIS"
EXT="pdf"
BINING=1
FIGURE_PATH="../plotting_out/obr/$EVO"

#which plots do we want to draw?
DO_RAW_R=0 #raw spectrum, R-dependency
DO_RAW_PTL=0 #raw spectrum, pTlead dependency
DO_AREA=0 #jet area
DO_DPT=0 #delta-pT
DO_RM=0 #Response Matrix
DO_EFFI=1 #jet reconstruction efficiency
DO_FINAL=0 #final plots (spectra, RAA, RCP, ratios R1/R2, pTlead1/pTlead2)
DO_FINAL_BIASED=0 #final plots, biased pp baseline in RAA
DO_COMPARE=0 #comparison plots with theory

#create output dirs
mkdir -p $FIGURE_PATH/results/comparison
mkdir -p $FIGURE_PATH/results/central
mkdir -p $FIGURE_PATH/results/peripheral
mkdir -p $FIGURE_PATH/uncorrected
mkdir -p $FIGURE_PATH/area
mkdir -p $FIGURE_PATH/dpT
mkdir -p $FIGURE_PATH/effi
mkdir -p $FIGURE_PATH/RM


PTLEAD_CENT="5.0 6.0 7.0"
PTLEAD_PERI="4.0 5.0 6.0 7.0"

#jet area and rho
if [ $DO_AREA -eq 1 ]; then
root -l <<EOF
.L plot_whatever.C
plot_area_and_rho(cent, 0, 0, "$EVO","$LABEL","$EXT")
.q
EOF
fi

for SYS in "peri" "cent"
do

if [ $SYS == "peri" ]; then
	PTLEAD_ARR=$PTLEAD_PERI
else
	PTLEAD_ARR=$PTLEAD_CENT
fi

#raw spectrum - R dependence
if [ $DO_RAW_R -eq 1 ]; then
root -l <<EOF	
.L plot_whatever.C
plot_measured_R(0.0, $SYS, "$EVO" ,"$LABEL", "$EXT")
.q
EOF
fi


for R in 0.2 0.3 0.4
do
#raw spectrum - pTlead dependence
if [ $DO_RAW_PTL -eq 1 ]; then
root -l <<EOF
.L plot_whatever.C
plot_measured_pTlead($R, $SYS, "$EVO", "$LABEL", "$EXT")
.q
EOF
fi


#delta-pT histograms
if [ $DO_DPT -eq 1 ]; then
root -l <<EOF
.L plot_whatever.C
plot_dpT($R, 0, 0, $SYS,"$EVO","$LABEL","$EXT")
.q
EOF
fi

#Response Matrix and its Y projection
if [ $DO_RM -eq 1 ]; then
root -l <<EOF
.L plot_whatever.C
plot_RM($R, 5.0, 0, $SYS, "$EVO", "$LABEL", "$EXT")
.q
EOF
fi


#Response Matrix and its Y projection
if [ $DO_EFFI -eq 1 ]; then
root -l <<EOF
.L plot_whatever.C
plot_epsilon($R, 5.0, $SYS, "$EVO","$LABEL","$EXT")
.q
EOF
fi
done #R loop


for PTLEAD in `echo $PTLEAD_ARR`
do

#final results
if [ $DO_FINAL -eq 1 ]; then
root -l <<EOF
.L plot_FINAL.C
plot_FINAL($PTLEAD, $SYS, "$EVO", $BINING, 0, 0, 0, 0, "$LABEL","$EXT")
.q
EOF
fi

#final results, biased pythia
if [ $DO_FINAL_BIASED -eq 1 ]; then
root -l <<EOF
.L plot_FINAL.C
plot_FINAL($PTLEAD, $SYS, "$EVO", $BINING, 0, 0, 0, 1, "$LABEL","$EXT")
.q
EOF
fi
done #PTLEAD
done #SYS


if [ $DO_COMPARE -eq 1 ]; then
for PTLEAD in `echo $PTLEAD_CENT`
do
#comparison with theory - RAA, RCP
cd RAA_RCP
root -l <<EOF
.L RAA_RCP.C
RAA_RCP($PTLEAD, $BINING,"$EVO", "$LABEL",0,"$EXT")
.q
EOF

#comparison with theory - RAA, RCP, biased PYTHIA
root -l <<EOF
.L RAA_RCP.C
RAA_RCP($PTLEAD, $BINING,"$EVO", "$LABEL",1,"$EXT")
.q
EOF
cd ..

#comparison with theory - R1/R2 ratios
cd RR_ratio
root -l <<EOF
.L R2R4.C
R2R4($PTLEAD,$BINING, "$EVO","$LABEL","$EXT")
.q
EOF
cd ..

done
fi
