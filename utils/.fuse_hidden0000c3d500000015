//calculate error of a ratio through a MC propagation
void calc_sigma_ratio(Double_t mu1, Double_t mu2, Double_t sigma1, Double_t sigma2, Double_t &sigma_up, Double_t &sigma_down, Int_t N=100)
{
   if(sigma2>0 && mu2>0){
   //cout<<"calculating ratio error"<<endl;
   Double_t left=mu1/mu2-5*(sigma1+sigma2);
   Double_t right=mu1/mu2+5*(sigma1+sigma2);
   TH1D* hratio=new TH1D("hratio","",100,left,right);
   Double_t mu;
   for(int i=0; i<N; i++)
   {
      Double_t rnd1=gRandom->Gaus(mu1,sigma1);
      Double_t rnd2=gRandom->Gaus(mu2,sigma2);
      Double_t xi=0;
      if(rnd2>0 || rnd2<0) xi=rnd1/rnd2;
      hratio->Fill(xi);
      mu+=xi/N;
   }
   //fit
TF1* gauss1=new TF1("gauss1","[0]*TMath::Exp(-((x-[1])*(x-[1]))/(2*[2]*[2]))",-10,10);
TF1* gauss2=new TF1("gauss2","[0]*TMath::Exp(-((x-[1])*(x-[1]))/(2*[2]*[2]))",-10,10);

gauss1->SetParameter(0,N);
gauss1->FixParameter(1,mu);
gauss1->SetParameter(2,(sigma1/mu1+sigma2/mu2)*mu);
gauss1->SetLineColor(kRed);

gauss2->SetParameter(0,N);
gauss2->FixParameter(1,mu);
gauss2->SetParameter(2,(sigma1/mu1+sigma2/mu2)*mu);
gauss1->SetLineColor(kBlue);

hratio->Fit(gauss2,"","",mu,right);
hratio->Fit(gauss1,"","",left,mu);  

//cout<<"left, mu, right: "<<left<<" "<<mu<<" "<<right<<endl;

//hratio->Draw("");

sigma_down=gauss1->GetParameter(2);
sigma_up=gauss2->GetParameter(2);

//cout<<"sigma_up: "<<sigma_up<<"sigma_down: "<<sigma_down<<endl;

delete hratio;
delete gauss1;
delete gauss2;
return;
   }
   
   else
   {
      sigma_down=sigma1;
      sigma_up=sigma1;
      return;
   }
}

//rebin histogram "hIN" to the same binning as "hTEMPLATE" and give it the name "name"
//CAUTION: hIN needs to have finer or equal bins as the hTEMPLATE!!!!
//if the input histogram is normalized by the bin width, set input_scaled_to_width=1, otherwise use input_scaled_to_width=0
TH1D* rebinhisto(TH1D* hIN, TH1D* hTEMPLATE, TString name, bool input_scaled_to_width=1)
{  
	TH1D* hOLD=(TH1D*) hIN->Clone("hold");
	hOLD->Sumw2();
	const int nbns=401;
	for(int i=0; i<4; i++)
	{
		if(hOLD->GetNbinsX()<nbns)continue;
		hOLD->Rebin(2);
		if(input_scaled_to_width)hOLD->Scale(0.5);
	}
	Double_t yield[nbns];
	Double_t error[nbns];
	Double_t binwidth[nbns];
	TH1D* hNEW=(TH1D*) hTEMPLATE->Clone(name.Data());
	hNEW->Reset("MICE");
	hNEW->Sumw2();
	for(int bn_new=1;bn_new<hNEW->GetNbinsX()+1;bn_new++){
		int bincount=0;
		for(int bn_old=1; bn_old<hOLD->GetNbinsX()+1;bn_old++)
		{
			if(hNEW->FindBin(hOLD->GetBinCenter(bn_old))!=bn_new)continue;
			//cout<<"bin new "<<bn_new<<" pT: "<<hNEW->GetBinCenter(bn_new)<<"bin old "<<bn_old<<" pT: "<<hOLD->GetBinCenter(bn_old)<<endl;
			yield[bincount]=hOLD->GetBinContent(bn_old);
			error[bincount]=hOLD->GetBinError(bn_old);
			binwidth[bincount]=hOLD->GetBinWidth(bn_old);
			if(input_scaled_to_width)
			{
				yield[bincount]=yield[bincount]*binwidth[bincount];
				error[bincount]=error[bincount]*binwidth[bincount];
			}
			bincount++;
		}//loop over bins of original histogram
		if(bincount==0)continue;

		double yieldt=0;
		double errt2=0;
		float bwidth=0;
		for(int i=0;i<bincount;i++)
		{
			yieldt+=yield[i];
			errt2+=error[i]*error[i];
			bwidth+=binwidth[i];
		}

		if(input_scaled_to_width)
		{
			hNEW->SetBinContent(bn_new,yieldt/bwidth);
			hNEW->SetBinError(bn_new,TMath::Sqrt(errt2)/bwidth);
		}
		else
		{
			hNEW->SetBinContent(bn_new,yieldt);
			hNEW->SetBinError(bn_new,TMath::Sqrt(errt2));
		}
	}//loop over bins of new histogram 

	delete hOLD;
	return hNEW;
} 

//BIN CENTER CORRECTION - move center of the bin to the position of the mean pT (= move it left for falling spectra)
//the new center is calculated based on a fit to the spectrum
//works with arrays wich can be then used for a TGraph
void bin_shift_LR(TF1* FitF, int nbins, const double* xarr, const double* yarr, const double* xerr_low=0, const double* xerr_high=0, const double* yerr_low=0, const double* yerr_high=0,const int niter=5)
{
  cout<<"BIN CENTER CORRECTION"<<endl;
   for(int itr=1;itr<niter;itr++)
   {
   TGraphAsymmErrors* TGE=new TGraphAsymmErrors(nbins,xarr,yarr,xerr_low,xerr_high,yerr_low,yerr_high);
   TGE->Fit(FitF);
   
   
	//loop over points in graph
	for(int i=0; i<nbins; i++) 
	{
	Double_t xleft=xarr[i]-xerr_low[i];
	Double_t xright=xarr[i]+xerr_high[i];
   
   Double_t int_left=FitF->Integral(xleft,xarr[i]);
   Double_t int_right=FitF->Integral(xarr[i],xright);
   double int_ratio=int_right/int_left;
      if(itr==niter-1) cout<<"bin"<<i<<" integral ratio:"<<int_ratio<<endl;
   xarr[i]=xleft+(xarr[i]-xleft)*int_ratio;
   xerr_low[i]=xarr[i]-xleft;
   xerr_high[i]=xright-xarr[i];
   }//loop over points in graph
	
	/*
	if(itr==niter-1)
   {
      TCanvas *cfit=new TCanvas("cfit","cfit",10,10,800,600);
   cfit->cd();
   TGE->DrawClone("ap");
   FitF->DrawClone("same");
   }
	*/
   }//loop over iterations
	return;
}

//BIN CENTER CORRECTION - move center of the bin up or down so a smooth function going through the new centers has the same integral as the original bins
//works with a histogram
TH1D* bin_shift_UD_hist(TF1* FitF, TH1D* hIN,TString outname="", const int niter=5, const float alpha=0.5)
{
   if(outname=="")outname=Form("%s_%s",hIN->GetTitle(),"recentered");
   TH1D* hNEW=(TH1D*) hIN->Clone(outname);
   for(int itr=1;itr<=niter;itr++)
   {  
      TH1D* htemp=(TH1D*)hNEW->Clone("htemp");
      hNEW->Reset("MICE");
      htemp->Fit(FitF);
      int nbins=htemp->GetNbinsX();
      for(int i=1; i<=nbins; i++) 
      {
         double yIN=hIN->GetBinContent(i);
         double yNEW=htemp->GetBinContent(i);
         if(!yNEW>0)continue;
         double error=htemp->GetBinError(i);
         double width=htemp->GetBinWidth(i);
         double xleft=htemp->GetBinLowEdge(i);
         double xright=xleft+width;
         double xcenter=htemp->GetBinCenter(i);
         
         double intH=yIN*width;
         //double intHNEW=yIN*width;
         double intF=FitF->Integral(xleft,xright);
         
         //scale y so bin center matches the fit function (just in case the fit was unable to go through all bin centers)
         double yf=FitF->Eval(xcenter);
         double prescale=1+(yf/yNEW-1)*alpha;
         yNEW=yNEW*prescale;
         
         //scale y up or down
         double scale=1+(intH/intF-1)*alpha;
         yNEW=yNEW*scale;
         error=error*scale;
         hNEW->SetBinContent(i,yNEW);
         hNEW->SetBinError(i,error);
         
        // if(xcenter>30 && xcenter<40)
           // cout<<"iteration: "<<itr<<" original y: "<<yIN<<" new y: "<<yNEW<<" integral f: "<<intF<<" integral h: "<<intH<<" ratio H/F: "<<intH/intF<<" scale: "<<scale<<endl;
      }//bin loop
      delete htemp;
   }//iteration loop
   return hNEW;
}




