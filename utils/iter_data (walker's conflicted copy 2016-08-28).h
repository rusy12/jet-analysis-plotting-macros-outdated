Int_t diter_data_normal[20][4][5][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning][test]

/*
Int_t diter_data_AuAu[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_v2[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_2u1g[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_m5[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_p5[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
*/
#include "optIter/optIter_data_normal_Camb_bin1.txt"
#include "optIter/optIter_data_normal_chi2_bin1.txt"
#include "optIter/optIter_data_normal_KS_bin1.txt"

#include "optIter/optIter_data_peripheral_normal_Camb_bin1.txt"
#include "optIter/optIter_data_peripheral_normal_chi2_bin1.txt"
#include "optIter/optIter_data_peripheral_normal_KS_bin1.txt"

#include "optIter/optIter_data_normal_Camb_bin2.txt"
#include "optIter/optIter_data_normal_chi2_bin2.txt"
#include "optIter/optIter_data_normal_KS_bin2.txt"

#include "optIter/optIter_data_peripheral_normal_Camb_bin2.txt"
#include "optIter/optIter_data_peripheral_normal_chi2_bin2.txt"
#include "optIter/optIter_data_peripheral_normal_KS_bin2.txt"

#include "optIter/optIter_data_normal_Camb_bin3.txt"
#include "optIter/optIter_data_normal_chi2_bin3.txt"
#include "optIter/optIter_data_normal_KS_bin3.txt"

#include "optIter/optIter_data_peripheral_normal_Camb_bin3.txt"
#include "optIter/optIter_data_peripheral_normal_chi2_bin3.txt"
#include "optIter/optIter_data_peripheral_normal_KS_bin3.txt"

#include "optIter/optIter_data_normal_Camb_bin4.txt"
#include "optIter/optIter_data_normal_chi2_bin4.txt"
#include "optIter/optIter_data_normal_KS_bin4.txt"

#include "optIter/optIter_data_peripheral_normal_Camb_bin4.txt"
#include "optIter/optIter_data_peripheral_normal_chi2_bin4.txt"
#include "optIter/optIter_data_peripheral_normal_KS_bin4.txt"

/*
#include "optIter/optIter_data_normal_Camb_bin2.txt"
#include "optIter/optIter_data_normal_chi2_bin2.txt"
#include "optIter/optIter_data_normal_KS_bin2.txt"
#include "optIter/optIter_data_normal_Camb_bin3.txt"
#include "optIter/optIter_data_normal_chi2_bin3.txt"
#include "optIter/optIter_data_normal_KS_bin3.txt"
#include "optIter/optIter_data_normal_Camb_bin4.txt"
#include "optIter/optIter_data_normal_chi2_bin4.txt"
#include "optIter/optIter_data_normal_KS_bin4.txt"

#include "optIter/optIter_data_v2_Camb_bin1.txt"
#include "optIter/optIter_data_v2_chi2_bin1.txt"
#include "optIter/optIter_data_v2_KS_bin1.txt"
#include "optIter/optIter_data_v2_Camb_bin2.txt"
#include "optIter/optIter_data_v2_chi2_bin2.txt"
#include "optIter/optIter_data_v2_KS_bin2.txt"
#include "optIter/optIter_data_v2_Camb_bin3.txt"
#include "optIter/optIter_data_v2_chi2_bin3.txt"
#include "optIter/optIter_data_v2_KS_bin3.txt"
#include "optIter/optIter_data_v2_Camb_bin4.txt"
#include "optIter/optIter_data_v2_chi2_bin4.txt"
#include "optIter/optIter_data_v2_KS_bin4.txt"

#include "optIter/optIter_data_AuAu_Camb_bin1.txt"
#include "optIter/optIter_data_AuAu_chi2_bin1.txt"
#include "optIter/optIter_data_AuAu_KS_bin1.txt"
#include "optIter/optIter_data_AuAu_Camb_bin2.txt"
#include "optIter/optIter_data_AuAu_chi2_bin2.txt"
#include "optIter/optIter_data_AuAu_KS_bin2.txt"
#include "optIter/optIter_data_AuAu_Camb_bin3.txt"
#include "optIter/optIter_data_AuAu_chi2_bin3.txt"
#include "optIter/optIter_data_AuAu_KS_bin3.txt"
#include "optIter/optIter_data_AuAu_Camb_bin4.txt"
#include "optIter/optIter_data_AuAu_chi2_bin4.txt"
#include "optIter/optIter_data_AuAu_KS_bin4.txt"

#include "optIter/optIter_data_2u1g_Camb_bin1.txt"
#include "optIter/optIter_data_2u1g_chi2_bin1.txt"
#include "optIter/optIter_data_2u1g_KS_bin1.txt"
#include "optIter/optIter_data_2u1g_Camb_bin2.txt"
#include "optIter/optIter_data_2u1g_chi2_bin2.txt"
#include "optIter/optIter_data_2u1g_KS_bin2.txt"
#include "optIter/optIter_data_2u1g_Camb_bin3.txt"
#include "optIter/optIter_data_2u1g_chi2_bin3.txt"
#include "optIter/optIter_data_2u1g_KS_bin3.txt"
#include "optIter/optIter_data_2u1g_Camb_bin4.txt"
#include "optIter/optIter_data_2u1g_chi2_bin4.txt"
#include "optIter/optIter_data_2u1g_KS_bin4.txt"

#include "optIter/optIter_data_m5_Camb_bin1.txt"
#include "optIter/optIter_data_m5_chi2_bin1.txt"
#include "optIter/optIter_data_m5_KS_bin1.txt"
#include "optIter/optIter_data_m5_Camb_bin2.txt"
#include "optIter/optIter_data_m5_chi2_bin2.txt"
#include "optIter/optIter_data_m5_KS_bin2.txt"
#include "optIter/optIter_data_m5_Camb_bin3.txt"
#include "optIter/optIter_data_m5_chi2_bin3.txt"
#include "optIter/optIter_data_m5_KS_bin3.txt"
#include "optIter/optIter_data_m5_Camb_bin4.txt"
#include "optIter/optIter_data_m5_chi2_bin4.txt"
#include "optIter/optIter_data_m5_KS_bin4.txt"

#include "optIter/optIter_data_p5_Camb_bin1.txt"
#include "optIter/optIter_data_p5_chi2_bin1.txt"
#include "optIter/optIter_data_p5_KS_bin1.txt"
#include "optIter/optIter_data_p5_Camb_bin2.txt"
#include "optIter/optIter_data_p5_chi2_bin2.txt"
#include "optIter/optIter_data_p5_KS_bin2.txt"
#include "optIter/optIter_data_p5_Camb_bin3.txt"
#include "optIter/optIter_data_p5_chi2_bin3.txt"
#include "optIter/optIter_data_p5_KS_bin3.txt"
#include "optIter/optIter_data_p5_Camb_bin4.txt"
#include "optIter/optIter_data_p5_chi2_bin4.txt"
#include "optIter/optIter_data_p5_KS_bin4.txt"
*/

	/*
		//CENTRAL 0-10%
      //R=0.2
		//pTlead>5
		//Bayes
		//prior|R|pTlead|Unf|cent|binning
		diter_data[2][0][2][0][0][0]=2;//2,3,4
		diter_data[4][0][2][0][0][0]=4;//4,5
		diter_data[5][0][2][0][0][0]=3;//3,4,5
		diter_data[6][0][2][0][0][0]=3;//2,3,4,5
		//SVD
		diter_data[2][0][2][1][0][0]=4;//4,5
		diter_data[4][0][2][1][0][0]=4;//4,5
		diter_data[5][0][2][1][0][0]=4;//4,5
		diter_data[6][0][2][1][0][0]=4;//4,5,6
		//pTlead>7
		//Bayes
		diter_data[2][0][3][0][0][0]=2;
		diter_data[4][0][3][0][0][0]=3;
		diter_data[5][0][3][0][0][0]=3;
		diter_data[6][0][3][0][0][0]=3;
		//SVD
		diter_data[2][0][3][1][0][0]=3;//3,4,5
		diter_data[4][0][3][1][0][0]=3;//3,4
		diter_data[5][0][3][1][0][0]=4;//4,5
		diter_data[6][0][3][1][0][0]=3;//3,4,5
      //R=0.3
		//pTlead>5
		//Bayes
		diter_data[2][1][2][0][0][0]=3;//3,4,5
		diter_data[4][1][2][0][0][0]=4;//4,5,6
		diter_data[5][1][2][0][0][0]=3;//3,4,5
		diter_data[6][1][2][0][0][0]=4;//4,5
		//SVD
		diter_data[2][1][2][1][0][0]=4;//3,4,5,6
		diter_data[4][1][2][1][0][0]=4;//3,4,5
		diter_data[5][1][2][1][0][0]=4;//4,5
		diter_data[6][1][2][1][0][0]=4;//4,5,6
		//pTlead>7
		//Bayes
		diter_data[2][1][3][0][0][0]=2;//2,3,4
		diter_data[4][1][3][0][0][0]=3;//3,4,5
		diter_data[5][1][3][0][0][0]=2;//2,3,4
		diter_data[6][1][3][0][0][0]=3;//3,4,5
		//SVD
		diter_data[2][1][3][1][0][0]=3;//3,4,5
		diter_data[4][1][3][1][0][0]=4;//3,4,5
		diter_data[5][1][3][1][0][0]=4;//3,4,5
		diter_data[6][1][3][1][0][0]=4;//3,4,5
						
      //R=0.4
		//pTlead>5
		//Bayes
		diter_data[2][2][2][0][0][0]=2;//2,3,4,5,6
		diter_data[4][2][2][0][0][0]=5;//5,6,7
		diter_data[5][2][2][0][0][0]=4;//3,4,5
		diter_data[6][2][2][0][0][0]=6;//6,7
		//SVD
		diter_data[2][2][2][1][0][0]=3;//2,3,4,5
		diter_data[4][2][2][1][0][0]=3;//3,4,5
		diter_data[5][2][2][1][0][0]=3;//3,4,5
		diter_data[6][2][2][1][0][0]=3;//3,4,5
		//pTlead>7
		//Bayes
		diter_data[2][2][3][0][0][0]=3;//3,4,5
		diter_data[4][2][3][0][0][0]=3;//3,4,5
		diter_data[5][2][3][0][0][0]=2;//2,3,4,5
		diter_data[6][2][3][0][0][0]=4;//4,5
		//SVD
		diter_data[2][2][3][1][0][0]=3;//3,4
		diter_data[4][2][3][1][0][0]=3;//3,4,5
		diter_data[5][2][3][1][0][0]=4;//4,5
		diter_data[6][2][3][1][0][0]=3;//3,4,5
		
		//PERIPHERAL 60-80%
      //R=0.2
		//pTlead>3
		//prior|R|pTlead|Unf|cent
		diter_data[2][0][0][0][1][0]=2;
		diter_data[4][0][0][0][1][0]=2;
		diter_data[5][0][0][0][1][0]=2;
		diter_data[6][0][0][0][1][0]=2;
		diter_data[2][0][0][1][1][0]=4;
		diter_data[4][0][0][1][1][0]=4;
		diter_data[5][0][0][1][1][0]=4;
		diter_data[6][0][0][1][1][0]=4;
		//pTlead>4
		diter_data[2][0][1][0][1][0]=2;
		diter_data[4][0][1][0][1][0]=2;
		diter_data[5][0][1][0][1][0]=2;
		diter_data[6][0][1][0][1][0]=2;
		diter_data[2][0][1][1][1][0]=4;
		diter_data[4][0][1][1][1][0]=4;
		diter_data[5][0][1][1][1][0]=4;
		diter_data[6][0][1][1][1][0]=4;
      //R=0.3
		//pTlead>3
		diter_data[2][1][0][0][1][0]=2;
		diter_data[4][1][0][0][1][0]=2;
		diter_data[5][1][0][0][1][0]=2;
		diter_data[6][1][0][0][1][0]=2;
		diter_data[2][1][0][1][1][0]=4;
		diter_data[4][1][0][1][1][0]=4;
		diter_data[5][1][0][1][1][0]=4;
		diter_data[6][1][0][1][1][0]=4;
		//pTlead>4
		diter_data[2][1][1][0][1][0]=2;
		diter_data[4][1][1][0][1][0]=2;
		diter_data[5][1][1][0][1][0]=2;
		diter_data[6][1][1][0][1][0]=2;
		diter_data[2][1][1][1][1][0]=4;
		diter_data[4][1][1][1][1][0]=4;
		diter_data[5][1][1][1][1][0]=4;
		diter_data[6][1][1][1][1][0]=4;
      //R=0.4
		//pTlead>3
		diter_data[2][2][0][0][1][0]=2;
		diter_data[4][2][0][0][1][0]=2;
		diter_data[5][2][0][0][1][0]=2;
		diter_data[6][2][0][0][1][0]=2;
		diter_data[2][2][0][1][1][0]=4;
		diter_data[4][2][0][1][1][0]=4;
		diter_data[5][2][0][1][1][0]=4;
		diter_data[6][2][0][1][1][0]=4;
		//pTlead>4
		diter_data[2][2][1][0][1][0]=2;
		diter_data[4][2][1][0][1][0]=2;
		diter_data[5][2][1][0][1][0]=2;
		diter_data[6][2][1][0][1][0]=2;
		diter_data[2][2][1][1][1][0]=4;
		diter_data[4][2][1][1][1][0]=4;
		diter_data[5][2][1][1][1][0]=4;
		diter_data[6][2][1][1][1][0]=4;

*/
