//prior|R|pTlead|unfolding|centrality|binning|test
//R=0.3
//pTlead=4
//Bayes
diter_data_p5[2][1][4][0][1][4][1]=-1;
diter_data_p5[4][1][4][0][1][4][1]=-1;
diter_data_p5[5][1][4][0][1][4][1]=-1;
diter_data_p5[6][1][4][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=48
diter_data_p5[7][1][4][0][1][4][1]=-1;
diter_data_p5[8][1][4][0][1][4][1]=-1;
diter_data_p5[9][1][4][0][1][4][1]=-1;
diter_data_p5[10][1][4][0][1][4][1]=-1;
diter_data_p5[11][1][4][0][1][4][1]=-1;
diter_data_p5[12][1][4][0][1][4][1]=-1;
diter_data_p5[13][1][4][0][1][4][1]=-1;
diter_data_p5[14][1][4][0][1][4][1]=-1;
diter_data_p5[15][1][4][0][1][4][1]=-1;
//SVD
diter_data_p5[2][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.9, curvature=41
diter_data_p5[4][1][4][1][1][4][1]=4; //backf_vs_meas=0.0, iter_change=0.2, curvature=36
diter_data_p5[5][1][4][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.4, curvature=47
diter_data_p5[6][1][4][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.7, curvature=49
diter_data_p5[7][1][4][1][1][4][1]=-1;
diter_data_p5[8][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=39
diter_data_p5[9][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=46
diter_data_p5[10][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.8, curvature=45
diter_data_p5[11][1][4][1][1][4][1]=4; //backf_vs_meas=0.0, iter_change=0.3, curvature=41
diter_data_p5[12][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=43
diter_data_p5[13][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=43
diter_data_p5[14][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.8, curvature=41
diter_data_p5[15][1][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=41
//pTlead=5
//Bayes
diter_data_p5[2][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=38
diter_data_p5[4][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=30
diter_data_p5[5][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=28
diter_data_p5[6][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=26
diter_data_p5[7][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.2, curvature=41
diter_data_p5[8][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=33
diter_data_p5[9][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=28
diter_data_p5[10][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=36
diter_data_p5[11][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=33
diter_data_p5[12][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=31
diter_data_p5[13][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=37
diter_data_p5[14][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=35
diter_data_p5[15][1][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=34
//SVD
diter_data_p5[2][1][5][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=29
diter_data_p5[4][1][5][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.3, curvature=33
diter_data_p5[5][1][5][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=29
diter_data_p5[6][1][5][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=40
diter_data_p5[7][1][5][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=34
diter_data_p5[8][1][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.4, curvature=35
diter_data_p5[9][1][5][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=35
diter_data_p5[10][1][5][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.6, curvature=33
diter_data_p5[11][1][5][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=28
diter_data_p5[12][1][5][1][1][4][1]=3; //backf_vs_meas=0.3, iter_change=0.1, curvature=35
diter_data_p5[13][1][5][1][1][4][1]=3; //backf_vs_meas=0.3, iter_change=0.6, curvature=39
diter_data_p5[14][1][5][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=25
diter_data_p5[15][1][5][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=33
//pTlead=6
//Bayes
diter_data_p5[2][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=30
diter_data_p5[4][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=22
diter_data_p5[5][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=21
diter_data_p5[6][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=19
diter_data_p5[7][1][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=30
diter_data_p5[8][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=24
diter_data_p5[9][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=21
diter_data_p5[10][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=27
diter_data_p5[11][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=25
diter_data_p5[12][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=23
diter_data_p5[13][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=27
diter_data_p5[14][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=26
diter_data_p5[15][1][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=26
//SVD
diter_data_p5[2][1][6][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.4, curvature=29
diter_data_p5[4][1][6][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=21
diter_data_p5[5][1][6][1][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.9, curvature=28
diter_data_p5[6][1][6][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=28
diter_data_p5[7][1][6][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=28
diter_data_p5[8][1][6][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=19
diter_data_p5[9][1][6][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=25
diter_data_p5[10][1][6][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.4, curvature=26
diter_data_p5[11][1][6][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=24
diter_data_p5[12][1][6][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=24
diter_data_p5[13][1][6][1][1][4][1]=2; //backf_vs_meas=0.3, iter_change=0.9, curvature=13
diter_data_p5[14][1][6][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=20
diter_data_p5[15][1][6][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=26
//R=0.5
//pTlead=4
//Bayes
diter_data_p5[2][3][4][0][1][4][1]=-1;
diter_data_p5[4][3][4][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=48
diter_data_p5[5][3][4][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=42
diter_data_p5[6][3][4][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=37
diter_data_p5[7][3][4][0][1][4][1]=-1;
diter_data_p5[8][3][4][0][1][4][1]=-1;
diter_data_p5[9][3][4][0][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=48
diter_data_p5[10][3][4][0][1][4][1]=-1;
diter_data_p5[11][3][4][0][1][4][1]=-1;
diter_data_p5[12][3][4][0][1][4][1]=-1;
diter_data_p5[13][3][4][0][1][4][1]=-1;
diter_data_p5[14][3][4][0][1][4][1]=-1;
diter_data_p5[15][3][4][0][1][4][1]=-1;
//SVD
diter_data_p5[2][3][4][1][1][4][1]=4; //backf_vs_meas=0.0, iter_change=0.3, curvature=40
diter_data_p5[4][3][4][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.4, curvature=38
diter_data_p5[5][3][4][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=38
diter_data_p5[6][3][4][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=40
diter_data_p5[7][3][4][1][1][4][1]=5; //backf_vs_meas=0.0, iter_change=0.1, curvature=49
diter_data_p5[8][3][4][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.6, curvature=42
diter_data_p5[9][3][4][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=48
diter_data_p5[10][3][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=41
diter_data_p5[11][3][4][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.5, curvature=49
diter_data_p5[12][3][4][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.7, curvature=48
diter_data_p5[13][3][4][1][1][4][1]=4; //backf_vs_meas=0.0, iter_change=0.4, curvature=41
diter_data_p5[14][3][4][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=41
diter_data_p5[15][3][4][1][1][4][1]=4; //backf_vs_meas=0.0, iter_change=0.2, curvature=42
//pTlead=5
//Bayes
diter_data_p5[2][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=36
diter_data_p5[4][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=23
diter_data_p5[5][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=21
diter_data_p5[6][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=19
diter_data_p5[7][3][5][0][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=43
diter_data_p5[8][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=28
diter_data_p5[9][3][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=26
diter_data_p5[10][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=31
diter_data_p5[11][3][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=30
diter_data_p5[12][3][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=29
diter_data_p5[13][3][5][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=33
diter_data_p5[14][3][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=32
diter_data_p5[15][3][5][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=32
//SVD
diter_data_p5[2][3][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.5, curvature=30
diter_data_p5[4][3][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=25
diter_data_p5[5][3][5][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=1.0, curvature=24
diter_data_p5[6][3][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=29
diter_data_p5[7][3][5][1][1][4][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=27
diter_data_p5[8][3][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=26
diter_data_p5[9][3][5][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.2, curvature=29
diter_data_p5[10][3][5][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.5, curvature=24
diter_data_p5[11][3][5][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=23
diter_data_p5[12][3][5][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=38
diter_data_p5[13][3][5][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.4, curvature=28
diter_data_p5[14][3][5][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.2, curvature=28
diter_data_p5[15][3][5][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=25
//pTlead=6
//Bayes
diter_data_p5[2][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=18
diter_data_p5[4][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
diter_data_p5[5][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
diter_data_p5[6][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=13
diter_data_p5[7][3][6][0][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.2, curvature=16
diter_data_p5[8][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=15
diter_data_p5[9][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=15
diter_data_p5[10][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=16
diter_data_p5[11][3][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=16
diter_data_p5[12][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=16
diter_data_p5[13][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=17
diter_data_p5[14][3][6][0][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=17
diter_data_p5[15][3][6][0][1][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=17
//SVD
diter_data_p5[2][3][6][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.3, curvature=18
diter_data_p5[4][3][6][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=17
diter_data_p5[5][3][6][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=19
diter_data_p5[6][3][6][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=19
diter_data_p5[7][3][6][1][1][4][1]=3; //backf_vs_meas=0.2, iter_change=0.6, curvature=9
diter_data_p5[8][3][6][1][1][4][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=15
diter_data_p5[9][3][6][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=14
diter_data_p5[10][3][6][1][1][4][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=17
diter_data_p5[11][3][6][1][1][4][1]=2; //backf_vs_meas=0.0, iter_change=1.0, curvature=18
diter_data_p5[12][3][6][1][1][4][1]=3; //backf_vs_meas=0.0, iter_change=0.2, curvature=14
diter_data_p5[13][3][6][1][1][4][1]=2; //backf_vs_meas=0.3, iter_change=0.9, curvature=11
diter_data_p5[14][3][6][1][1][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=15
diter_data_p5[15][3][6][1][1][4][1]=2; //backf_vs_meas=0.0, iter_change=1.0, curvature=19
