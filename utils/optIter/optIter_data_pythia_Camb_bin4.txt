//prior|R|pTlead|unfolding|centrality|binning|test
//R=0.3
//pTlead=5
//Bayes
diter_data_pythia[2][1][5][0][0][4][1]=-1;
diter_data_pythia[4][1][5][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
diter_data_pythia[5][1][5][0][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.0, curvature=12
diter_data_pythia[6][1][5][0][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.0, curvature=13
diter_data_pythia[7][1][5][0][0][4][1]=-1;
diter_data_pythia[8][1][5][0][0][4][1]=-1;
diter_data_pythia[9][1][5][0][0][4][1]=5; //backf_vs_meas=0.0, iter_change=0.0, curvature=18
diter_data_pythia[10][1][5][0][0][4][1]=-1;
diter_data_pythia[11][1][5][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=20
diter_data_pythia[12][1][5][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=21
diter_data_pythia[13][1][5][0][0][4][1]=-1;
diter_data_pythia[14][1][5][0][0][4][1]=-1;
diter_data_pythia[15][1][5][0][0][4][1]=-1;
//SVD
diter_data_pythia[2][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.5, curvature=13
diter_data_pythia[4][1][5][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=13
diter_data_pythia[5][1][5][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=14
diter_data_pythia[6][1][5][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=15
diter_data_pythia[7][1][5][1][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.7, curvature=25
diter_data_pythia[8][1][5][1][0][4][1]=4; //backf_vs_meas=0.0, iter_change=0.1, curvature=14
diter_data_pythia[9][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=11
diter_data_pythia[10][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=15
diter_data_pythia[11][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=13
diter_data_pythia[12][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=10
diter_data_pythia[13][1][5][1][0][4][1]=4; //backf_vs_meas=0.0, iter_change=0.3, curvature=14
diter_data_pythia[14][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=12
diter_data_pythia[15][1][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=11
//pTlead=6
//Bayes
diter_data_pythia[2][1][6][0][0][4][1]=-1;
diter_data_pythia[4][1][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=9
diter_data_pythia[5][1][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=10
diter_data_pythia[6][1][6][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=10
diter_data_pythia[7][1][6][0][0][4][1]=7; //backf_vs_meas=0.1, iter_change=0.0, curvature=14
diter_data_pythia[8][1][6][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=10
diter_data_pythia[9][1][6][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=12
diter_data_pythia[10][1][6][0][0][4][1]=4; //backf_vs_meas=0.0, iter_change=0.1, curvature=11
diter_data_pythia[11][1][6][0][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.0, curvature=12
diter_data_pythia[12][1][6][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
diter_data_pythia[13][1][6][0][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=11
diter_data_pythia[14][1][6][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
diter_data_pythia[15][1][6][0][0][4][1]=5; //backf_vs_meas=0.1, iter_change=0.0, curvature=13
//SVD
diter_data_pythia[2][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=11
diter_data_pythia[4][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=11
diter_data_pythia[5][1][6][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=12
diter_data_pythia[6][1][6][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=12
diter_data_pythia[7][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=15
diter_data_pythia[8][1][6][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=10
diter_data_pythia[9][1][6][1][0][4][1]=4; //backf_vs_meas=0.2, iter_change=0.2, curvature=10
diter_data_pythia[10][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=11
diter_data_pythia[11][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=10
diter_data_pythia[12][1][6][1][0][4][1]=5; //backf_vs_meas=0.0, iter_change=0.3, curvature=32
diter_data_pythia[13][1][6][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=9
diter_data_pythia[14][1][6][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=10
diter_data_pythia[15][1][6][1][0][4][1]=5; //backf_vs_meas=0.0, iter_change=0.2, curvature=32
//pTlead=7
//Bayes
diter_data_pythia[2][1][7][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=8
diter_data_pythia[4][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=9
diter_data_pythia[5][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=12
diter_data_pythia[6][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=14
diter_data_pythia[7][1][7][0][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=8
diter_data_pythia[8][1][7][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=10
diter_data_pythia[9][1][7][0][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=13
diter_data_pythia[10][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=8
diter_data_pythia[11][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=13
diter_data_pythia[12][1][7][0][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=12
diter_data_pythia[13][1][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=9
diter_data_pythia[14][1][7][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=12
diter_data_pythia[15][1][7][0][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.1, curvature=12
//SVD
diter_data_pythia[2][1][7][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=7
diter_data_pythia[4][1][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=10
diter_data_pythia[5][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=10
diter_data_pythia[6][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=10
diter_data_pythia[7][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.6, curvature=6
diter_data_pythia[8][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=10
diter_data_pythia[9][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.6, curvature=9
diter_data_pythia[10][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=10
diter_data_pythia[11][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=9
diter_data_pythia[12][1][7][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=7
diter_data_pythia[13][1][7][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=1.0, curvature=9
diter_data_pythia[14][1][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=8
diter_data_pythia[15][1][7][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=7
//R=0.5
//pTlead=5
//Bayes
diter_data_pythia[2][3][5][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=9
diter_data_pythia[4][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=6
diter_data_pythia[5][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=9
diter_data_pythia[6][3][5][0][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.1, curvature=12
diter_data_pythia[7][3][5][0][0][4][1]=-1;
diter_data_pythia[8][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=7
diter_data_pythia[9][3][5][0][0][4][1]=2; //backf_vs_meas=0.3, iter_change=0.1, curvature=26
diter_data_pythia[10][3][5][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=5
diter_data_pythia[11][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=12
diter_data_pythia[12][3][5][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=24
diter_data_pythia[13][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=7
diter_data_pythia[14][3][5][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=12
diter_data_pythia[15][3][5][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=18
//SVD
diter_data_pythia[2][3][5][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=10
diter_data_pythia[4][3][5][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=6
diter_data_pythia[5][3][5][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=10
diter_data_pythia[6][3][5][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=13
diter_data_pythia[7][3][5][1][0][4][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=19
diter_data_pythia[8][3][5][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=7
diter_data_pythia[9][3][5][1][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.3, curvature=23
diter_data_pythia[10][3][5][1][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=5
diter_data_pythia[11][3][5][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=15
diter_data_pythia[12][3][5][1][0][4][1]=-1;
diter_data_pythia[13][3][5][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=7
diter_data_pythia[14][3][5][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=12
diter_data_pythia[15][3][5][1][0][4][1]=-1;
//pTlead=6
//Bayes
diter_data_pythia[2][3][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=6
diter_data_pythia[4][3][6][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=7
diter_data_pythia[5][3][6][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.0, curvature=10
diter_data_pythia[6][3][6][0][0][4][1]=2; //backf_vs_meas=0.3, iter_change=0.1, curvature=14
diter_data_pythia[7][3][6][0][0][4][1]=5; //backf_vs_meas=0.2, iter_change=0.1, curvature=3
diter_data_pythia[8][3][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=8
diter_data_pythia[9][3][6][0][0][4][1]=5; //backf_vs_meas=0.3, iter_change=0.1, curvature=16
diter_data_pythia[10][3][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=5
diter_data_pythia[11][3][6][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=12
diter_data_pythia[12][3][6][0][0][4][1]=5; //backf_vs_meas=0.2, iter_change=0.1, curvature=15
diter_data_pythia[13][3][6][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=7
diter_data_pythia[14][3][6][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=12
diter_data_pythia[15][3][6][0][0][4][1]=2; //backf_vs_meas=0.3, iter_change=0.1, curvature=17
//SVD
diter_data_pythia[2][3][6][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=6
diter_data_pythia[4][3][6][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=7
diter_data_pythia[5][3][6][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=8
diter_data_pythia[6][3][6][1][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.3, curvature=10
diter_data_pythia[7][3][6][1][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.8, curvature=4
diter_data_pythia[8][3][6][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=8
diter_data_pythia[9][3][6][1][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.5, curvature=15
diter_data_pythia[10][3][6][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=5
diter_data_pythia[11][3][6][1][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=10
diter_data_pythia[12][3][6][1][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.4, curvature=14
diter_data_pythia[13][3][6][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=1.0, curvature=7
diter_data_pythia[14][3][6][1][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.1, curvature=10
diter_data_pythia[15][3][6][1][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.3, curvature=12
//pTlead=7
//Bayes
diter_data_pythia[2][3][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=5
diter_data_pythia[4][3][7][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=8
diter_data_pythia[5][3][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=12
diter_data_pythia[6][3][7][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=16
diter_data_pythia[7][3][7][0][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.1, curvature=2
diter_data_pythia[8][3][7][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=9
diter_data_pythia[9][3][7][0][0][4][1]=3; //backf_vs_meas=0.3, iter_change=0.2, curvature=22
diter_data_pythia[10][3][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=6
diter_data_pythia[11][3][7][0][0][4][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=13
diter_data_pythia[12][3][7][0][0][4][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=19
diter_data_pythia[13][3][7][0][0][4][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=7
diter_data_pythia[14][3][7][0][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=12
diter_data_pythia[15][3][7][0][0][4][1]=2; //backf_vs_meas=0.3, iter_change=0.2, curvature=17
//SVD
diter_data_pythia[2][3][7][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=5
diter_data_pythia[4][3][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=6
diter_data_pythia[5][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.2, curvature=6
diter_data_pythia[6][3][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=7
diter_data_pythia[7][3][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.6, curvature=4
diter_data_pythia[8][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=6
diter_data_pythia[9][3][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.6, curvature=9
diter_data_pythia[10][3][7][1][0][4][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=6
diter_data_pythia[11][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.3, curvature=7
diter_data_pythia[12][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.5, curvature=8
diter_data_pythia[13][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=6
diter_data_pythia[14][3][7][1][0][4][1]=3; //backf_vs_meas=0.0, iter_change=0.3, curvature=6
diter_data_pythia[15][3][7][1][0][4][1]=3; //backf_vs_meas=0.1, iter_change=0.4, curvature=7
