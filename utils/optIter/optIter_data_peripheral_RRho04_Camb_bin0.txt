//prior|R|pTlead|unfolding|centrality|binning|test
//R=0.3
//pTlead=4
//Bayes
diter_data_RRho04[2][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[4][1][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
diter_data_RRho04[5][1][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
diter_data_RRho04[6][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[7][1][4][0][1][0][1]=3; //backf_vs_meas=0.0, iter_change=0.0, curvature=1
diter_data_RRho04[8][1][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
diter_data_RRho04[9][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[10][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[11][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[12][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[13][1][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[14][1][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
diter_data_RRho04[15][1][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
//SVD
diter_data_RRho04[2][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.7, curvature=0
diter_data_RRho04[4][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=0
diter_data_RRho04[5][1][4][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=0.5, curvature=0
diter_data_RRho04[6][1][4][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.6, curvature=1
diter_data_RRho04[7][1][4][1][1][0][1]=5; //backf_vs_meas=0.1, iter_change=0.4, curvature=1
diter_data_RRho04[8][1][4][1][1][0][1]=4; //backf_vs_meas=0.2, iter_change=0.4, curvature=0
diter_data_RRho04[9][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[10][1][4][1][1][0][1]=4; //backf_vs_meas=0.2, iter_change=0.8, curvature=1
diter_data_RRho04[11][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.5, curvature=0
diter_data_RRho04[12][1][4][1][1][0][1]=4; //backf_vs_meas=0.2, iter_change=0.4, curvature=1
diter_data_RRho04[13][1][4][1][1][0][1]=5; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[14][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.8, curvature=0
diter_data_RRho04[15][1][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.6, curvature=0
//pTlead=5
//Bayes
diter_data_RRho04[2][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[4][1][5][0][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=0
diter_data_RRho04[5][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[6][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[7][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[8][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[9][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[10][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[11][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[12][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[13][1][5][0][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.1, curvature=0
diter_data_RRho04[14][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[15][1][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
//SVD
diter_data_RRho04[2][1][5][1][1][0][1]=4; //backf_vs_meas=0.2, iter_change=0.3, curvature=0
diter_data_RRho04[4][1][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=0
diter_data_RRho04[5][1][5][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=0
diter_data_RRho04[6][1][5][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=1.0, curvature=0
diter_data_RRho04[7][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=1
diter_data_RRho04[8][1][5][1][1][0][1]=3; //backf_vs_meas=0.2, iter_change=0.4, curvature=0
diter_data_RRho04[9][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[10][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[11][1][5][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.6, curvature=1
diter_data_RRho04[12][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[13][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[14][1][5][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.6, curvature=1
diter_data_RRho04[15][1][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
//pTlead=6
//Bayes
diter_data_RRho04[2][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[4][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[5][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[6][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[7][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[8][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=0
diter_data_RRho04[9][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[10][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[11][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=0
diter_data_RRho04[12][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[13][1][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[14][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[15][1][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
//SVD
diter_data_RRho04[2][1][6][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=0.8, curvature=0
diter_data_RRho04[4][1][6][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=0
diter_data_RRho04[5][1][6][1][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.9, curvature=0
diter_data_RRho04[6][1][6][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[7][1][6][1][1][0][1]=3; //backf_vs_meas=0.3, iter_change=0.6, curvature=0
diter_data_RRho04[8][1][6][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=0
diter_data_RRho04[9][1][6][1][1][0][1]=4; //backf_vs_meas=0.2, iter_change=0.1, curvature=0
diter_data_RRho04[10][1][6][1][1][0][1]=3; //backf_vs_meas=0.2, iter_change=0.3, curvature=0
diter_data_RRho04[11][1][6][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=0
diter_data_RRho04[12][1][6][1][1][0][1]=3; //backf_vs_meas=0.3, iter_change=0.2, curvature=0
diter_data_RRho04[13][1][6][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=0
diter_data_RRho04[14][1][6][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=0
diter_data_RRho04[15][1][6][1][1][0][1]=3; //backf_vs_meas=0.3, iter_change=0.1, curvature=0
//R=0.5
//pTlead=4
//Bayes
diter_data_RRho04[2][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[4][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[5][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[6][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=1
diter_data_RRho04[7][3][4][0][1][0][1]=3; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
diter_data_RRho04[8][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[9][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=1
diter_data_RRho04[10][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[11][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[12][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[13][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[14][3][4][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[15][3][4][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=1
//SVD
diter_data_RRho04[2][3][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=1
diter_data_RRho04[4][3][4][1][1][0][1]=3; //backf_vs_meas=0.2, iter_change=0.6, curvature=1
diter_data_RRho04[5][3][4][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=0
diter_data_RRho04[6][3][4][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=0
diter_data_RRho04[7][3][4][1][1][0][1]=5; //backf_vs_meas=0.1, iter_change=0.2, curvature=1
diter_data_RRho04[8][3][4][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.8, curvature=1
diter_data_RRho04[9][3][4][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=1
diter_data_RRho04[10][3][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.9, curvature=1
diter_data_RRho04[11][3][4][1][1][0][1]=3; //backf_vs_meas=0.3, iter_change=0.5, curvature=1
diter_data_RRho04[12][3][4][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.6, curvature=1
diter_data_RRho04[13][3][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.7, curvature=1
diter_data_RRho04[14][3][4][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.4, curvature=1
diter_data_RRho04[15][3][4][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=0.6, curvature=1
//pTlead=5
//Bayes
diter_data_RRho04[2][3][5][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[4][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[5][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[6][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[7][3][5][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.2, curvature=0
diter_data_RRho04[8][3][5][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[9][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[10][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[11][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[12][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[13][3][5][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[14][3][5][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[15][3][5][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
//SVD
diter_data_RRho04[2][3][5][1][1][0][1]=3; //backf_vs_meas=0.3, iter_change=0.4, curvature=1
diter_data_RRho04[4][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[5][3][5][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=1.0, curvature=0
diter_data_RRho04[6][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[7][3][5][1][1][0][1]=4; //backf_vs_meas=0.1, iter_change=0.3, curvature=0
diter_data_RRho04[8][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[9][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[10][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.5, curvature=0
diter_data_RRho04[11][3][5][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.8, curvature=0
diter_data_RRho04[12][3][5][1][1][0][1]=3; //backf_vs_meas=0.2, iter_change=0.2, curvature=1
diter_data_RRho04[13][3][5][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.4, curvature=0
diter_data_RRho04[14][3][5][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.7, curvature=0
diter_data_RRho04[15][3][5][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.7, curvature=1
//pTlead=6
//Bayes
diter_data_RRho04[2][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[4][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[5][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[6][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[7][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[8][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[9][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[10][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[11][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[12][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.1, curvature=0
diter_data_RRho04[13][3][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.1, curvature=0
diter_data_RRho04[14][3][6][0][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[15][3][6][0][1][0][1]=2; //backf_vs_meas=0.0, iter_change=0.0, curvature=0
//SVD
diter_data_RRho04[2][3][6][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=0.8, curvature=0
diter_data_RRho04[4][3][6][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.9, curvature=0
diter_data_RRho04[5][3][6][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.0, curvature=0
diter_data_RRho04[6][3][6][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[7][3][6][1][1][0][1]=3; //backf_vs_meas=0.2, iter_change=0.5, curvature=0
diter_data_RRho04[8][3][6][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=0
diter_data_RRho04[9][3][6][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.3, curvature=0
diter_data_RRho04[10][3][6][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=0.8, curvature=0
diter_data_RRho04[11][3][6][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=1.0, curvature=0
diter_data_RRho04[12][3][6][1][1][0][1]=3; //backf_vs_meas=0.1, iter_change=0.2, curvature=0
diter_data_RRho04[13][3][6][1][1][0][1]=2; //backf_vs_meas=0.2, iter_change=0.8, curvature=0
diter_data_RRho04[14][3][6][1][1][0][1]=2; //backf_vs_meas=0.1, iter_change=0.9, curvature=0
diter_data_RRho04[15][3][6][1][1][0][1]=2; //backf_vs_meas=0.3, iter_change=1.0, curvature=0
