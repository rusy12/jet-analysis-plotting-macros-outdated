Int_t diter_data_normal[20][4][7][2][3][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral/pp][binning][test]
Int_t diter_data_peripheral_normal[20][4][7][2][3][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral/pp][binning][test]
Int_t diter_data_pp_normal[20][4][7][2][3][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral/pp][binning][test]

/*
Int_t diter_data_p5[20][4][7][2][3][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral/pp][binning][test]
Int_t diter_data_m5[20][4][7][2][3][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral/pp][binning][test]

Int_t diter_data_AuAu[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_v2[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_2u1g[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_m5[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
Int_t diter_data_p5[20][3][4][2][2][5][3]; //optimal iterations [prior][R][pTlead][Bayes/SVD][central/peripheral][binning]
*/
#include "optIter/optIter_data_normal_Camb_bin0.txt"
#include "optIter/optIter_data_normal_Camb_bin1.txt"
#include "optIter/optIter_data_normal_Camb_bin2.txt"
#include "optIter/optIter_data_normal_Camb_bin3.txt"
#include "optIter/optIter_data_normal_Camb_bin4.txt"
//#include "optIter/optIter_data_normal_chi2_bin1.txt"
//#include "optIter/optIter_data_normal_KS_bin1.txt"

#include "optIter/optIter_data_peripheral_normal_Camb_bin0.txt"
#include "optIter/optIter_data_peripheral_normal_Camb_bin1.txt"
#include "optIter/optIter_data_peripheral_normal_Camb_bin2.txt"
#include "optIter/optIter_data_peripheral_normal_Camb_bin3.txt"
#include "optIter/optIter_data_peripheral_normal_Camb_bin4.txt"
//#include "optIter/optIter_data_peripheral_normal_chi2_bin1.txt"
//#include "optIter/optIter_data_peripheral_normal_KS_bin1.txt"

//#include "optIter/optIter_data_pp_normal_Camb_bin0.txt"
#include "optIter/optIter_data_pp_normal_Camb_bin1.txt"
#include "optIter/optIter_data_pp_normal_Camb_bin2.txt"
#include "optIter/optIter_data_pp_normal_Camb_bin3.txt"
#include "optIter/optIter_data_pp_normal_Camb_bin4.txt"

/*
#include "optIter/optIter_data_p5_Camb_bin0.txt"
#include "optIter/optIter_data_p5_Camb_bin1.txt"
#include "optIter/optIter_data_p5_Camb_bin2.txt"
#include "optIter/optIter_data_p5_Camb_bin3.txt"
#include "optIter/optIter_data_p5_Camb_bin4.txt"

#include "optIter/optIter_data_m5_Camb_bin0.txt"
#include "optIter/optIter_data_m5_Camb_bin1.txt"
#include "optIter/optIter_data_m5_Camb_bin2.txt"
#include "optIter/optIter_data_m5_Camb_bin3.txt"
#include "optIter/optIter_data_m5_Camb_bin4.txt"
*/
