#!/bin/bash
SUFFIX="omicron" #"newBinning2" #"eta" # 
TOYMODEL=0
PPBASE="PYTHIA" #PYTHIA | STAR
#TEST=1 # 0:chi2 1:Camberra 2: Kolmogorov-Smirnov 

for PPBIAS in 0 # 1: use pythia with pTlead cut as the pp baseline for RAA | 0: use PYTHIA with no pTlead cut
do
if [ $PPBIAS -eq 0 ]; then
	SUFFIX2=${SUFFIX}
else
	SUFFIX2="${SUFFIX}_ppBiased"
fi

#if [ $TEST -eq 0 ]; then
#	TESTDIR="chi2"
#elif [ $TEST -eq 1 ]; then
#	TESTDIR="Camb"
#else
#	TESTDIR="KS"
#fi


for BININGCH in 1 #0 1 4 #1 2 3 4 #2 #binning set
do
	for SYSTEM in  peri #cent #pp # 
do
	TOYDIR=""
	if [ $SYSTEM == cent ]; then #central collisions
		PTLEADCUTS="7"  #"5 6 7"
		RATIOS="RCP" #"RAA RR plpl RCP"
		if [ $TOYMODEL -eq 1 ]; then
		RATIOS="closure RAA RR plpl"
		TOYDIR="toymodel/"
		fi
		CENTDIR="central"
	elif [ $SYSTEM == peri ]; then #peripheral collisions
		PTLEADCUTS=6 #"4 5 6 7"
		RATIOS="RAA RR plpl"
		CENTDIR="peripheral"
	else #pp data
		PTLEADCUTS="5" #"4 5 6" 
		RATIOS="RAA RR plpl"
		CENTDIR="pp"
	fi

	OUTDIR="../plotting_out/systematics/${TOYDIR}${CENTDIR}/${SUFFIX2}/bining$BININGCH"
	mkdir -p $OUTDIR
	echo "WARNING: Removing files in $OUTDIR"
	#rm $OUTDIR/*.root
	rm $OUTDIR/*_pT6.root


	for RAT_T in `echo $RATIOS` 
	do
		#if [ $RAT_T == RCP ]; then
		#	PTLEADCUTS="5 6"
		#elif [ $SYSTEM == cent ]; then 
		#	PTLEADCUTS="5 6 7"
		#fi

	for R in 0.2 0.3 0.4 #0.5
	do
	for PTLEAD in `echo $PTLEADCUTS`
	do
	for UNC_T in unfolding correlated #normalization
	do
		root -q -l -b systematics.C\($UNC_T,$RAT_T,$SYSTEM,$R,$PTLEAD,\"$SUFFIX\",$BININGCH,\"normal\",$PPBASE,$PPBIAS,$TOYMODEL,1\)
	done #uncert
	done #pTlead
	done #R
	done #ratio
done #centrality
done #bining
done #pythia bias
