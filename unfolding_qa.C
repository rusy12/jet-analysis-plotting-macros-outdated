#include "utils/common.h"
#include "utils/statistic_tests.C"
#include "utils/utils.C"

void unfolding_qa(systems system=cent, /*int binning=0,*/bool doToymodel=0, TString uname="eta")
{

   bool ppHT=1; //0: use only MB data for pp | 1: use combined MB+HT data for pp
	const int nunf=2; //number of unfoldings (usually 2: SVD+Bayes)
	//TString unfoldings_name[]={"Bayes","SVD"};
	const int nprior=16; //16
	TString prior_type[]={"","flat","pythia","powlaw3","powlaw45","powlaw5","powlaw55","tsalis_1","tsalis_2","tsalis_3","tsalis_4","tsalis_5","tsalis_6","tsalis_7","tsalis_8","tsalis_9"};
	int prior_start=2;
	TString nbins="VAR";
	//const int nR=3; //moved to utils/common.h
	//const float Rs[]={0.2,0.3,0.4,0.5}; //moved to utils/common.h
	const int nbinnings=3; //not used now, only one binning is used, set by "binning=x"
	const int binnings[]={0,4,1,2,3}; //see above comment
	int ptl_start=5;
   int ptl_end=7; 
   if(system==peri)
   {
      ptl_start=7;
      ptl_end=7;
   }
	else if(system==pp) //pp data
	{
      ptl_start=3;
      ptl_end=6;
	}
	//const float pTls[]={0,1,3,4,5,6,7}; moved to utils/common.h
	int maxiter=7; //number of unfolding iterations
	const int vsize=maxiter; //size of the output vectors
	float RAAtoy=(system==cent) ? 0.5 : 0.7; //toymodel RAA of hard jet spectrum
	int nsuf=12; //5
	const TString suff[]={"u","g","normal","p5","m5","v2","pp","pythia","trcuts2","RRho02","RRho04", "nrem-1","tofp5","tofm5"};
   if(doToymodel)
	{
		nsuf=1;
	}
	
	float mincutoff[]={10,10,10}; //low-pT cutoff for backfolding test
	float mincutof_curv[]={0,7,7}; //mincutoff for curvature test for central,  peripheral and pp collisons
	float maxcutoff[]={30,22,25}; //high-pT cutoff
	
	TString dtype="data";
	if(doToymodel)dtype="toy";
	
	TString trigg_cent="MB";
	if(doToymodel) trigg_cent="toymodel";
	if(system==peri) trigg_cent+="_peripheral";
	else if(system==pp) 
    {
        if(ppHT) trigg_cent+="HT_pp";
        else trigg_cent+="_pp";
    }
	TString toyraa="";
	if(doToymodel) toyraa=Form("_RAA%.1lf",RAAtoy);
	
	/*
		Color_t colorList[]={kBlack,kBlue,kRed,kGreen,kMagenta,kOrange};
	  TH1 *frame = new TH1I("frame", "", 1000, 0, +100);
	  frame->GetXaxis()->SetRangeUser(0, 30);
  frame->GetYaxis()->SetRangeUser(0.1, 3E6);
   TCanvas *cspectra = new TCanvas("cspectra","cspectra",10,10,900,600);
  cspectra->cd();
   cspectra->SetLogy();
	frame->DrawCopy("");*/
	
	for(int bn=0;bn<nbinnings;bn++)
	{
		int binning=binnings[bn];
		cout<<"binning: "<<binnings[bn]<<"("<<bn+1<<"/"<<nbinnings<<")"<<endl;
	for(int sf=0; sf<nsuf;sf++)
	{
		if(doToymodel && sf>0)continue;
		cout<<"type: "<<suff[sf].Data()<<"("<<sf+1<<"/"<<nsuf<<")"<<endl;
	for(int r=0; r<nR; r++){
		//if(r==0)continue;
		//if(r==2)continue;
		cout<<"R="<<Rs[r]<<endl;
      for(int ptl=ptl_start; ptl<=ptl_end; ptl++){
			cout<<"   pTlead>"<<pTls[ptl]<<endl;
            if(pTls[ptl]==2) continue;
			for(int unf=0; unf<nunf; unf++){
				cout<<"      "<<unfoldings_name[unf].Data()<<endl;
				for(int prior=prior_start; prior<nprior; prior++){
               if (prior==3)continue;
					//cout<<"         "<<prior_type[prior].Data()<<endl;

	TString cortype=(system==pp) ? "dete" : "BGD";
	TString wrkdir = Form("../plotting_out/root/%s/%s/Unfolded_R%.1lf_%s_%sbins_bining%i_%s%s_%s_%s",trigg_cent.Data(),uname.Data(),Rs[r],unfoldings_name[unf].Data(),nbins.Data(),binning,cortype.Data(),toyraa.Data(),uname.Data(),suff[sf].Data());
	TString filein = Form("%s/%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", wrkdir.Data(),prior_type[prior].Data(), Rs[r], pTls[ptl]);
	TFile* funfolding = new TFile(filein.Data(), "OPEN");
	TString fileout = Form("%s/%s/unfoldingQA_R%.1lf_pTthresh%.1lf.root", wrkdir.Data(),prior_type[prior].Data(), Rs[r], pTls[ptl]);
	TFile* fqa = new TFile(fileout.Data(), "RECREATE");
	TFile* ftrue;
	TFile* fjets;

	//chi2 test
	TVectorD chi2change(vsize); //succeessive iterations
	TVectorD chi2backf(vsize);
	TVectorD chi2backf_trunk(vsize);// backfolded vs measured

	//Kolmogorov Smirnov test
   TVectorD KSchange(vsize); //succeessive iterations
	TVectorD KSbackf(vsize);
	TVectorD KSbackf_trunk(vsize);// backfolded vs measured
	
	//Camberra test
	TVectorD Cambchange(vsize); //succeessive iterations
	TVectorD Cambbackf(vsize);
	TVectorD Cambbackf_trunk(vsize);// backfolded vs measured
	
	//Curvature test
	TVectorD Curvature(vsize);
	
	//toymodel closure test
	TVectorD Toyclosure_Camb(vsize);
	TVectorD Toyclosure_KS(vsize);
	TVectorD Toyclosure_chi2(vsize);
	
	TH1D* htrue;
	int nevtrue;
	int nevunf;
	if(doToymodel)
	{
		//unfolded vs true (TOYMODEl only)

		fjets=new TFile(Form("../plotting_out/root/%s/%s/root_RAA%.1lf_%s/histos_jets_R%.1lf_pTcut0.2.root",trigg_cent.Data(),uname.Data(),RAAtoy,uname.Data(),Rs[r]),"OPEN");
		TH1I* hevents=(TH1I*)fjets->Get("hevents");
		nevunf=hevents->GetEntries();
		delete hevents;
		fjets->Close();

		ftrue=new TFile(Form("../plotting_out/root/%s/%s/root_RAA%.1lf_%s/jetonly_R%.1lf_pTcut0.2.root",trigg_cent.Data(),uname.Data(),RAAtoy,uname.Data(),Rs[r]),"OPEN");
		TH1I* hevt=(TH1I*)ftrue->Get("hevents");
		nevtrue=hevt->GetEntries();
		delete hevt;
		
		funfolding->cd();
		TDirectoryFile* diter_1 = (TDirectoryFile*)funfolding->Get("iter1");
		TH1D* hunf_1=(TH1D*)diter_1->Get("hunfolded");
		
		ftrue->cd();
		TH2D *hPtRecpTleading = (TH2D*)ftrue->Get("fhPtRecpTleading");
		Int_t firstbin = hPtRecpTleading->GetXaxis()->FindBin(pTls[ptl]);
		Int_t lastbin = hPtRecpTleading->GetNbinsX();
		TH1D *htemp = (TH1D*) hPtRecpTleading->ProjectionY("htemp", firstbin, lastbin);
		//float test_cont=htemp->GetBinContent(htemp->FindBin(10));
		//cout<<"test content:"<<test_cont<<endl;
		htemp->Scale((float) nevunf/nevtrue/*,"width"*/);
		htrue = (TH1D*) rebinhisto(htemp,hunf_1,"hjettruth",0);
		//cout<<"n events true: "<<nevtrue<<"n events unf: "<<nevunf<<endl;
		delete htemp;
		delete hPtRecpTleading;

	}
	for(int iter=1; iter<maxiter; iter++){ //first iteration: iter=0, we start from 2nd iteration (iter=1), since the first iteration is the same as the prior function (in SVD case)
						TDirectoryFile* dinput = (TDirectoryFile*)funfolding->Get("input");
						TDirectoryFile* diter_n = (TDirectoryFile*)funfolding->Get(Form("iter%i",iter));
						TDirectoryFile* diter_m = (TDirectoryFile*)funfolding->Get(Form("iter%i",iter-1));
						
						
						//sucessive iterations
						TH1D* hunf_n=(TH1D*)diter_n->Get("hunfolded");
						TH1D* hunf_m=(TH1D*)diter_m->Get("hunfolded");
						chi2change[iter]=chi2_test(hunf_n,hunf_m,maxcutoff[system]);
						Cambchange[iter]=Canberra_test(hunf_n,hunf_m,maxcutoff[system]);
						KSchange[iter]=KS_test(hunf_n,hunf_m,maxcutoff[system]);
						
						//backfolded vs measured
						TH1D* hbf=(TH1D*) diter_n->Get("hbackfolded");
						TH1D* hmeasured=(TH1D*) dinput->Get("hmeasured");
		
						chi2backf[iter]=chi2_test(hbf,hmeasured,maxcutoff[system]);
						Cambbackf[iter]=Canberra_test(hbf,hmeasured,maxcutoff[system]);
						KSbackf[iter]=KS_test(hbf,hmeasured,maxcutoff[system]);
						
						//test of trunkated distributions
						chi2backf_trunk[iter]=chi2_test(hbf,hmeasured,maxcutoff[system],mincutoff[system]);
						Cambbackf_trunk[iter]=Canberra_test(hbf,hmeasured,maxcutoff[system],mincutoff[system]);
						KSbackf_trunk[iter]=KS_test(hbf,hmeasured,maxcutoff[system],mincutoff[system]);
						
						//curvature test
						Curvature[iter]=Curv_test(hunf_n,maxcutoff[system],mincutof_curv[system]);
						/*cout<<iter<<" curvature:"<<Curvature[iter]<<endl;
						if(iter==4){
						hunf_n->SetLineColor(colorList[bn]);
						hunf_n->DrawCopy("same");}*/
						
						if(doToymodel) //toymodel closure test
						{
							//hunf_n->Scale(1.0,"width");
							float tc=Canberra_test(hunf_n,htrue,maxcutoff[system],5);
							float tk=KS_test(hunf_n,htrue,maxcutoff[system],5);
							float tx=chi2_test(hunf_n,htrue,maxcutoff[system],5);
							Toyclosure_Camb[iter]=tc;
							Toyclosure_KS[iter]=tk;
							Toyclosure_chi2[iter]=tx;
							//cout<<iter+1<<" Toy test: "<<tc<<endl;
						}

					}//iteration

               //write the output
               fqa->cd();
					
					chi2change.Write("chi2change");
					Cambchange.Write("Cambchange");
					KSchange.Write("KSchange");
					
					chi2backf.Write("chi2backf");
					Cambbackf.Write("Cambbackf");
					KSbackf.Write("KSbackf");
					
					chi2backf_trunk.Write("chi2backf_trunk");
					Cambbackf_trunk.Write("Cambbackf_trunk");
					KSbackf_trunk.Write("KSbackf_trunk");
					
					Curvature.Write("Curvature");
					
					if(doToymodel)
					{
						Toyclosure_Camb.Write("Toyclosure_Camb");
						Toyclosure_KS.Write("Toyclosure_KS");
						Toyclosure_chi2.Write("Toyclosure_chi2");
					}
						fqa->Close();
					funfolding->Close();
				if(doToymodel)ftrue->Close();
				}//prior
			}//unfolding
		}//pTlead
	}//R
	}//suffix
	}//binning

	
}
